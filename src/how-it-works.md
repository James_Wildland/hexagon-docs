# How Hexagon works

This article describes Hexagon's rationale and mechanics, if you want to know how to use the app, clik [here][using-the-app].

With the recent [Ethereum switch from the Proof-of-Work to the Proof-of-Stake][switch-to-POS] consensus mechanism - [Golem Foundation][golem-foundation], the organization developing Hexagon, made the decision to become a [validator in the network][validator]. The Foundation will stake up to 100 000 ETH to secure the network and help it reach a consensus.

In return for staking its Ether and becoming a validator, the Foundation will receive a steady stream of [continuous rewards directly from the protocol][staking-rewards]. Part of the staking rewards will be transferred to the Hexagon community, whose members will decide through individual actions and community polls how these funds will be distributed.

Community members will be able to choose between claiming part of the reward for themselves and donating it to eligible public goods causes.

In the process, Golem Foundation will gather useful empirical evidence on how users participate in decentralized governance. This evidence will inform [our search for effective decentralized governance solutions][degov], and further the development of the [User-Defined Organization][UDO] project.

Participation in the Hexagon community will be open to any person who holds [GLM tokens][glm] (an ERC-20 token, native to the [Golem Network][golem-network]) and stakes a certain minimum amount of their tokens into the Hexagon contract.

You will be able to stake as low as 1 GLM, but to claim rewards and vote in the Hexagon community polls, you will have to stake at least 100 GLMs for one epoch (the length of which is currently set at 90 days).

Staking in this context means time-locking your GLM. The tokens staked by Hexagon users will not be pooled together with other users’ funds, and the whole GLM staking set-up is non-custodial. Users will be able to withdraw their tokens at any time. However, if they do so before the end of a given epoch, they will not be able to claim any rewards nor participate in community decisions regarding the distribution of funds.

The amount of ETH rewards transferred by the Golem Foundation to Hexagon will be determined by [the fraction of the entire GLM supply staked by Hexagon users][technical-outline]. The more GLM is staked in the Hexagon contract, the higher the amount of the Foundation’s staking rewards that will be turned over to the Hexagon community for distribution.

The distribution of ETH rewards transferred by the foundation to Hexagon will be determined through an iterative process repeated over periods called epochs. Currently, the length of a Hexagon epoch is set to 90 days, but this may change in the future.

Before an epoch starts, members of the Hexagon community will discuss and vote on the list of potential donation recipients chosen from a list of eligible public goods projects maintained by the Golem Foundation (To read more on the eligibility criteria and how you can submit a project, see section [Propose a project][propose-a-project]).

Each user will also have to decide how much GLM to stake in the upcoming epoch. Those wishing to have a say in how the ETH transferred by the Golem Foundation to Hexagon is going to be distributed will have to time-lock at least 100 GLM into the Hexagon contract (lower stakes are possible but will not confer any rewards or governance power to a user). Higher stakes will result in higher rewards and more voting power in token-weighted polls.

Users who want to receive ETH rewards have to keep their GLM staked throughout the epoch. If they withdraw their GLM before the epoch ends they will not be able to claim any rewards, or donate their part of the staking proceeds to eligible causes.

At the beginning of the epoch, the amount of ETH rewards to be turned over to the Hexagon community for distribution will be determined based on [the fraction of the entire GLM supply staked by Hexagon users in the previous epoch][technical-outline]. We call this amount the Total Rewards (or TR for short).

Every community member who staked at least 100 GLM in the previous epoch, and did not withdraw their tokens from the Hexagon contract before the epoch ended, will be eligible to claim User Rewards (UR) during the subsequent epoch. A User Reward will be calculated [proportionally to the amount of GLM staked by a user in the previous epoch][technical-outline] (the more GLM you staked, the higher your UR).

At the beginning of each epoch community members with valid stakes will have a two-week decision window to determine what they want to do with their User Rewards. They can choose between claiming the rewards for themselves or donating it in full or in part to one or more eligible causes.

If a user decides to donate at least some part of their User Rewards to a community-selected project, the impact of their donations will be magnified through Hexagon’s Matching Rewards (MR). The MR is going to be determined as a [function of the amount of GLM staked by a user, and the proportion of the User Rewards they donated to eligible projects][technical-outline].

Once this decision window is closed community members who decided to keep their rewards will be able to withdraw their share of the ETH rewards into their wallets, and the matched donations will be distributed among projects that reached a predetermined threshold of community contributions. In the MVP stage, this threshold has been tentatively set at 10%.

The rewards that were neither claimed nor allocated by users, as well as all donations below the threshold, will be transferred back to the Golem Foundation.

To find out more about how this mechanism is implemented in the Hexagon App, and how you can participate in the project, read the [Using the App][using-the-app] section.

To learn more about the math we are using to calculate Total Rewards, User Rewards, and Matched Rewards, read Hexagon’s [Technical Outline][technical-outline].


[switch-to-POS]: https://ethereum.org/en/upgrades/merge/ 
[golem-foundation]: https://golem.foundation
[validator]: https://ethereum.org/en/developers/docs/consensus-mechanisms/pos/#validators
[staking-rewards]: https://ethereum.org/en/staking/
[degov]: https://wildland.io/2021/12/16/udo1.html
[UDO]: https://wildland.io/2021/12/16/udo2.html
[glm]: https://www.golem.network/glm 
[golem-network]: https://golem.network
[technical-outline]: ./technical-outline.md
[propose-a-project]: ./propose-a-project.md
[using-the-app]: ./using-the-app.md
