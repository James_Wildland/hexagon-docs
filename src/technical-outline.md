# Technical outline

The following document describes the math behind the distribution of staking proceeds in Hexagon. If you would rather read a math-free description of how Hexagon works click [here][how-it-works].

**Notation:** 
- $t$, $t+1$, $...$ denotes time/subsequent epochs;
- $i$ denotes Hexagon users. In particular it is assumed that in epoch $t$ there are $N_{t}$ participants. $N_{t}$ may vary from epoch to epoch;
- $k$ denotes eligible causes. In particular it is assumed that in epoch $t$ there are $K_{t}$ causes purposes eligible to receive donations. $K_{t}$ may vary from epoch to epoch.

If user $j$ decides to participate in Hexagon in epoch $t$, the GLM amount she staked is denoted as $GLMGE_{j,t}$.

## Total Rewards

The amount of funds transferred by the [Golem Foundation][golem-foundation] to the Hexagon contract - Total Rewards or $TR_{t}$ - depends on how much of the total GLM supply ($S[GLM]_{t}$) is staked in the Hexagon smart contract in epoch $t$. 

$$TR_{t} = SP_{t} * \sqrt{\frac{\sum_{i}^{N_{t}}GLMGE_{i,t}}{S[GLM]_t}}$$

The Foundation will not retain any fraction of the yield (i.e., $TR_{t} = SP_{t}$) only in the theoretical scenario when the entire supply of GLM ($S[GLM]_{t}$) is staked, i.e., $\sum_{i=1}^{N_{t}}GLMGE_{i,t} = S[GLM]_{t}$

## User Rewards

Individual rewards that a Hexagon user can claim or donate in epoch $t$ are $UR_{j,t}$.

In every epoch $t$ Hexagon participants are entitled to claim a share of rewards which is proportional to the ratio of the amount of GLM they staked in the Hexagon smart contract ($GLMGE_{j,t}$) to the entire supply of GLM ($S[GLM]_{t}$).

$$UR_{j,t} =\frac{GLMGE_{j,t}}{S[GLM]{t}} * SP_{t}$$

At the aggregate level, the entire budget available for individual rewards $UR_{t}$ amounts to:

$$UR_{t} = \sum_{i=1}^{N_{t}}UR_{i,t}=\frac{\sum_{i=1}^{N_{t}}GLMGE_{i,t}}{S[GLM]{t}}*SP_{t}$$

Users may choose to claim only a fraction of the available individual reward and to donate the remaining available amount to one (or more) of the public good purposes/organizations. In every epoch $t$ there are $K_{t}$ potential beneficiaries. 

A user $j$ thus decides on the value of a vector of parameters $\alpha_{j,t}^{k}$, $k \in (1,...,K_{t})$ which represent the fraction of their individual reward in epoch $t$ they are willing to donate to purpose/organization $k$. Note that $\forall k \quad 0 \leq \alpha_{j,t}^{k} \leq 1$ and $\sum_{k=1}^{K_{t}} \alpha_{j,t}^{k} \leq 1$.

The individual reward claimed by the user ($CUR_{j,t}$) is thus equal to:
$$CUR_{j,t} = (1-\sum_{k=1}^{K_{t}}\alpha_{j,t}^{}k)UR_{j,t}$$

The aggregate amount of redeemed individual rewards ($CUR_{t}$) amounts to:
$$CUR_{t}=\sum_{i=1}^{N_{t}} CUR_{i,t}=\sum_{i=1}^{N_{t}} (1-\sum_{k=1}^{K_{t}}\alpha_{i,t}^{}k)UR_{i,t}$$

## Matched Rewards

If users decide not to claim their individual rewards, they can choose to donate it for one (or more) of the eligible causes. In this case, their individual contribution $k$ (in the amount of $\alpha_{j,t}^{k}UR_{j,t}$) is automatically increased by the matched rewards ($MR_{j,t}$).

At the aggregate level, the budget available for matched rewards in epoch $t$ ($MR_{t}$) amounts to the difference between the total amount of staking proceeds allocated to Hexagon in epoch $t$ ($TR_t$) and the total amount of user rewards ($UR_t$).

$$MR_{t}=TR_{t}-UR_{t}$$

In order to be eligible for receiving donations in epoch $t$, a purpose/organization must pass a predefined threshold of individual donations. Tentatively, it is proposed to set this threshold at the level of $\frac{1}{K_{t}}$. We thus introduce the parameter of the user's *effective allocation* decisions.

$$\alpha_{j,t}^{*k} = \alpha_{j,t}^{k} \Leftrightarrow  \frac{\sum_{i=1}^{N_t} \alpha_{i,t}^{k}UR_{i,t}}{\sum_{k=1}^{K_t} \sum_{i=1}^{N_t}\alpha_{i,t}^{k}UR_{i,t}} \geq \frac{1}{K_t}$$

$$\alpha_{j,t}^{*k} = 0 \Leftrightarrow  \frac{\sum_{i=1}^{N_t} \alpha_{i,t}^{k}UR_{i,t}}{\sum_{k=1}^{K_t} \sum_{i=1}^{N_t}\alpha_{i,t}^{k}UR_{i,t}} < \frac{1}{K_t}$$

At the level of an individual ($j$) the distribution scheme proposed for the MVP is based on the individual's share in the staking amount in epoch weighted by how much each user effectively donated to public good purposes/organizations instead of claiming the individual reward.

$$MR_{j,t} = \frac{GLMGE_{j,t}(\sum_{k=1}^{K_t}\alpha_{j,t}^{*k})}{\sum_{i}^{N_{t}}GLMGE_{i,t}(\sum_{k=1}{K_t}\alpha_{i,t}^{*k})}  MR_{t}$$

The fraction of $UR_t$ , which users allocate to purposes/organizations that do not pass the threshold, is transferred to Golem Foundation. The same applies to the $UR_t$ of users who make no decisions with respect to the available rewards.

## Example: How funding works from a Hexagon user’s perspective

Alice (denoted as $j$) is participating in Hexagon in epoch $t$.

She decides to claim/withdraw 30% of the available user reward ($UR_{j,t}$).

She also decides to split the remaining 70% between 4 organizations: 10% to organization A, 10% to organization B, 20% to organization, and 30% to organization D.

When other Hexagon participants have made their decisions, it turns out that only organizations C and D are eligible for funding, as organizations A and B did not reach the minimum threshold of contributions.

This results in the following distribution:

- 30% of Alice's user reward goes to her.

- 20% of Alice's user reward (i.e., allocations to organizations A [10%] and B [10%] that haven't passed the threshold) is transferred to Golem Foundation.

- 50% of Alices' individual rewards (i.e., allocations to organizations C [20%] and D [30%]) is donated to the recipients, proportionally increased by Alice's matched rewards: organization C receives 40% of the matched rewards $\frac{20\%}{20\%+30\%}$ and organization D receives 60% ($\frac{30\%}{20\%+30\%}$).

[how-it-works]: ./how-it-works.md
[golem-foundation]: https://golem.foundation 
