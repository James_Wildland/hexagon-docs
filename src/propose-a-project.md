# Propose a project

> :memo: **Note:** Hexagon is currently in its testing phase. We are not yet accepting submissions for potential donation recipients.

Hexagon is a community-driven platform for experiments in decentralized governance. During its MVP stage we will be using Hexagon to test various hypotheses related to sustainable financing of public goods and infrastructure. For every 90-day epoch, a range of public good projects will be chosen as potential donation recipients. 

Golem Foundation will be maintaining and curating the list of projects eligible to receive funds from the Hexagon reward pool, based on inputs from the Hexagon community. 

To be considered for inclusion, potential donation recipients should meet the following set of criteria:
- commitment to open-source technology and sharing results publicly;
- transparency about how exactly funding will be used;
- advancing values of freedom and privacy (no surveillance and handling of data);
- supporting decentralization in various fields, building [Web3 projects][web3];
- developing GLM/Golem Network-compatible projects;
- being a public good;
- having social proof, being recognizable in the area the project is being developed in.

Exclusion criteria:
- not receiving public funding;
- not being a financial (lending, investing, trading) product.

Every Hexagon user can submit proposals for new additions to the list of eligible projects, a change in the eligibility criteria, or a theme for an epoch. 

At first, all submissions will be made on a dedicated [Discord channel][discord-submission]. Later on, a special submission form will be made available to Hexagon's community members.

Once a cause or an organization is listed on the eligibility index, it can be selected as one of the potential beneficiaries in a given epoch (depending on its theme). There are no limits to the number of times a project can be a potential beneficiary.

The voting for donation recipients to be included in an epoch will happen on Hexagon's [Snapshot][snapshot]. 

Snapshot will have all approved projects subjected to community voting, with a selected number of projects being added to the Hexagon potential recipients list for a given epoch.

[web3]: https://ethereum.org/en/web3/
[discord-submission]: https://TBA
[snapshot]: https://snapshot.org/#/hexagonapp.eth 
